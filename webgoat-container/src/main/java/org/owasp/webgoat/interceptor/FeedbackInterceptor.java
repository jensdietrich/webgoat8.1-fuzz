package org.owasp.webgoat.interceptor;

import nz.ac.wgtn.cornetto.jee.rt.Constants;
import nz.ac.wgtn.cornetto.jee.rt.InvocationTracker;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Intercept requests in order to append a header with a unique id that can be used by a fuzzing client to pick up
 * information gathered via instrumentation.
 * @author jens dietrich
 */

@Component
public class FeedbackInterceptor implements HandlerInterceptor {

    private static final boolean LOG = false;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (LOG) {
            request.getServletContext().log("pre handle: " + request.getServletPath());
        }

        String path = request.getServletPath();
        boolean isApplicationRequest = !path.contains(Constants.FUZZING_FEEDBACK_PATH_TOKEN);
        if (isApplicationRequest) {
            if (request.getHeader(Constants.TAINTED_INPUT_HEADER) != null) {
                InvocationTracker.DEFAULT.registerTaintStrings(((HttpServletRequest) request).getHeader(Constants.TAINTED_INPUT_HEADER));
            }
            String ID = InvocationTracker.DEFAULT.startInvocationTracking();
            response.addHeader(Constants.FUZZING_FEEDBACK_TICKET_HEADER, ID);
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        if (LOG) {
            request.getServletContext().log("post handle: " + request.getServletPath());
        }

        String path = request.getServletPath();
        boolean isApplicationRequest = !path.contains(Constants.FUZZING_FEEDBACK_PATH_TOKEN);
        if (isApplicationRequest) {
            InvocationTracker.DEFAULT.finishInvocationTracking();
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        if (LOG) {
            request.getServletContext().log("after completion: " + request.getServletPath());
        }
    }
}
